## Description

This package calculates standard evapotranspiration data, ETo, and merges it into a netCDF file along with WRF precipitation, and temperature data.

The merged file is used as input data for the [Soil Moisture Estimator (SME)](https://gitlab.com/agrineer/sme)
application.
 
A GUI implementation of the SME is on the Agrineer.org 
[website](https://agrineer.org/sme/sme.php) and currently covers about half of the western United States, with a ~3km pixel resolution.

This package was developed using GNU/Linux Mint 17, but other 
Debian platforms are known to work (Ubuntu 12,14; Mint 18,19).

There are two class/main programs in this package:  

- eto.py -  Calculates standard evapotranspiration (ETo) using Weather, Research, and Forecasting (WRF) model data as input, accumulated hourly per day.

- merge.py - Selectively reads WRF meta and data variables (lat, long, min/max temp, rain) used for ETo calculation in "eto.py" class, then merges these variables with the output ETo data, to be used as input to the SME package. 

Data produced by "merge.py" are archived as input for the SME application [here](https://www.agrineer.org/downloads/sectors.php).

These programs are provided for review purposes on the data flow. They show how the ETo is calculated and merged with other data. They are made available in case you want to modify and run your own WRF/Eto runs. You can find WRF input namelist files for each sector used [here](https://www.agrineer.org/downloads/sectors.php).


## Dependencies

   - Python 3 versions of:
      - Numpy 
      - netCDF4
      - osgeo
    
   - Try:
   
           > pip3 numpy
           > pip3 netCDF4
           > pip3 osgeo

## Installation
   
   Determine or create a directory to house the ETo package, eg. /home/user/projects

   Change into this directory and either unpack or clone this project.  
     
           > cd /home/user/projects  
           
           > tar xvf eto.tar
           or   
           > git clone https://gitlab.com/agrineer/eto

## How to Run:

   * Must have output from a WRF run 
   * Implemented WRF data tree based on dates
   * Must review and change hardcoded paths

Run eto.py first,  
    
          > ./eto.py -s sectorname <-r date>
          
      Remember, the sector directory is hard coded in eto.py.  
      If run date is not given it will use yesterday's date.

   Then run mergy.py  
   
          > merge.py -s sectorname <-l True/False> <-r date>
   Set the -l flag to "True" for lat,long output. Default is False since static geo files used SME have the lat,long values.

   